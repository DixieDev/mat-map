mod export_palette;

use crate::export_palette::export_palette_png;
use clap::{App, Arg, SubCommand};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    fs::{File, OpenOptions},
    hash::{Hash, Hasher},
    io::{Read, Write},
    path::Path,
};
use hex::FromHex;

const VALID_HEX_CHARS: [char; 16] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
];

pub fn rgb_from_hex(hex: &str) -> [u8; 3] {
    <[u8; 3]>::from_hex(&hex[2..]).unwrap()
}

#[derive(Eq, PartialEq, Clone, Serialize, Deserialize)]
struct PropertyEntry {
    name: String,
    value: String,
}

impl PropertyEntry {
    fn new(name: &str, value: &str) -> Self {
        Self {
            name: name.to_string(),
            value: value.to_string(),
        }
    }
}

#[derive(Eq, PartialEq, Serialize, Deserialize)]
pub struct MatMapEntry {
    name: String,
    colour: String,
    properties: Vec<PropertyEntry>,
}

impl MatMapEntry {
    fn new(name: &str, colour: &str, properties: &[PropertyEntry]) -> Self {
        Self {
            name: name.to_string(),
            colour: colour.to_string(),
            properties: Vec::from(properties),
        }
    }

    fn name<'a>(&'a self) -> &'a str {
        &self.name
    }

    fn colour<'a>(&'a self) -> &'a str {
        &self.colour
    }

    fn clean_colour_string(colour: &str) -> Result<String, ()> {
        let colour = if colour.starts_with("0x") {
            colour[2..].to_string()
        } else {
            colour.to_string()
        };

        if colour.len() != 6 {
            eprintln!("Invalid number of characters in colour hex code. There should be 6 characters defining the colour, but {} were provided.", colour.len());
            return Err(());
        }

        let colour: String = colour.to_uppercase();
        for c in colour.chars() {
            if !VALID_HEX_CHARS.contains(&c) {
                eprintln!("Invalid character '{}' provided in hex code.", c);
                return Err(());
            }
        }

        Ok(format!("0x{}", colour))
    }

    fn find_property<'a>(&'a mut self, name: &str) -> Option<&'a mut PropertyEntry> {
        for property in self.properties.iter_mut() {
            if property.name == name {
                return Some(property);
            }
        }

        None
    }
}

impl Hash for MatMapEntry {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.colour.hash(state);
    }
}

#[derive(Serialize, Deserialize)]
struct MatMap {
    mappings: Vec<MatMapEntry>,
}

impl MatMap {
    fn new() -> Self {
        Self {
            mappings: Vec::new(),
        }
    }

    fn load_or_create(path: &Path) -> Self {
        // Deserialise mappings from file, or create new empty mapping object
        match File::open(path) {
            Ok(mut f) => {
                let mut mat_map_content = String::new();
                f.read_to_string(&mut mat_map_content).unwrap();
                serde_json::from_str(&mat_map_content).unwrap()
            }
            Err(_) => MatMap::new(),
        }
    }

    fn serialise(&self, path: &Path) {
        let out = serde_json::to_string_pretty(self).unwrap();

        match OpenOptions::new()
            .create(true)
            .write(true)
            .open(Path::new(path))
        {
            Ok(mut f) => f.write_all(out.as_bytes()).unwrap(),
            Err(_) => eprintln!(
                "Failed to open file '{}' for writing.",
                path.to_str().unwrap()
            ),
        }
    }

    fn show_mappings(&self, names: &[String], colours: &[String]) {
        let mut mappings = HashSet::new();
        for mapping in self.mappings.iter() {
            for name in names {
                if name.to_string() == mapping.name {
                    mappings.insert(mapping);
                    break;
                }
            }
            for colour in colours {
                if colour.to_string() == mapping.colour {
                    mappings.insert(mapping);
                    break;
                }
            }
        }

        for mapping in mappings.iter() {
            println!("{} : {}", mapping.name, mapping.colour);
        }
    }

    fn add_mapping(
        &mut self,
        name: &str,
        colour: &str,
        properties: &[PropertyEntry],
    ) -> Result<(), ()> {
        for mapping in self.mappings.iter() {
            if mapping.name == name {
                eprintln!(
                    "Cannot add new mapping '{}' as mapping already exists.",
                    name
                );
                return Err(());
            }
        }

        self.mappings
            .push(MatMapEntry::new(name, colour, properties));
        Ok(())
    }

    fn rename_mapping(&mut self, old_name: &str, new_name: &str) -> Result<(), ()> {
        for mapping in self.mappings.iter_mut() {
            if mapping.name == old_name {
                mapping.name = new_name.to_string();
                println!("{} : {}", mapping.name, mapping.colour);
                return Ok(());
            }
        }

        eprintln!("No mapping with the name '{}' could be found.", old_name);
        Err(())
    }

    fn recolour_mapping(&mut self, name: &str, colour: &str) -> Result<(), ()> {
        if let Some(mapping) = self.find_mapping(name) {
            mapping.colour = colour.to_string();
            println!("{} : {}", mapping.name, mapping.colour);
            Ok(())
        } else {
            Err(())
        }
    }

    fn edit_property(
        &mut self,
        mat_name: &str,
        prop_name: &str,
        prop_value: &str,
    ) -> Result<(), ()> {
        if let Some(mapping) = self.find_mapping(mat_name) {
            if let Some(property) = mapping.find_property(prop_name) {
                property.value = prop_value.to_string();
                return Ok(());
            }
            eprintln!(
                "No property named '{}' found in material '{}'",
                prop_name, mat_name
            );
        }

        Err(())
    }

    fn add_property(
        &mut self,
        mat_name: &str,
        prop_name: &str,
        prop_value: &str,
    ) -> Result<(), ()> {
        if let Some(mapping) = self.find_mapping(mat_name) {
            if let None = mapping.find_property(prop_name) {
                let property = PropertyEntry::new(prop_name, prop_value);
                mapping.properties.push(property);
                return Ok(());
            }
            eprintln!(
                "Property with name '{}' in material '{}' already exists",
                prop_name, mat_name
            );
        }

        Err(())
    }

    fn find_mapping<'a>(&'a mut self, name: &str) -> Option<&'a mut MatMapEntry> {
        for mapping in self.mappings.iter_mut() {
            if mapping.name == name {
                return Some(mapping);
            }
        }

        eprintln!("No mapping with the name '{}' could be found.", name);
        None
    }
}

fn main() {
    let matches = App::new("Material Mapper")
        .version("1.0")
        .about("Maps material names to colours")
        .arg(
            Arg::with_name("MAP_FILE")
                .index(1)
                .required(true)
                .help("The material mapping file to work with"),
        )
        .subcommand(
            SubCommand::with_name("export-palette")
                .about("Export a material map to a PNG file for use as a colour palette")
                .arg(
                    Arg::with_name("PNG_FILE")
                        .required(true)
                        .help("The PNG file to write to"),
                ),
        )
        .subcommand(
            SubCommand::with_name("show")
                .about("Display material mappings")
                .arg(
                    Arg::with_name("name")
                        .short("n")
                        .long("name")
                        .multiple(true)
                        .takes_value(true)
                        .help("The names of materials to display"),
                )
                .arg(
                    Arg::with_name("colour")
                        .short("c")
                        .long("colour")
                        .multiple(true)
                        .takes_value(true)
                        .help("The colour hex codes of materials to display"),
                ),
        )
        .subcommand(
            SubCommand::with_name("add")
                .about("Add a material mapping to an archive")
                .arg(
                    Arg::with_name("NAME")
                        .index(1)
                        .required(true)
                        .help("The name of the material within the mapping"),
                )
                .arg(
                    Arg::with_name("COLOUR")
                        .index(2)
                        .required(true)
                        .help("The hex code of the material's colour"),
                )
                .arg(
                    Arg::with_name("property")
                        .short("p")
                        .long("property")
                        .multiple(true)
                        .number_of_values(2)
                        .value_names(&["name", "value"])
                        .help("A flexible property mapping to include on the new material"),
                )
        )
        .subcommand(
            SubCommand::with_name("rename")
                .about("Rename an existing material mapping")
                .arg(
                    Arg::with_name("CURRENT_NAME")
                        .index(1)
                        .required(true)
                        .help("The current name of the material"),
                )
                .arg(
                    Arg::with_name("NEW_NAME")
                        .index(2)
                        .required(true)
                        .help("The new name for the material"),
                ),
        )
        .subcommand(
            SubCommand::with_name("recolour")
                .about("Change the colour of an existing material mapping")
                .arg(
                    Arg::with_name("NAME")
                        .index(1)
                        .required(true)
                        .help("The name of the material to change"),
                )
                .arg(
                    Arg::with_name("COLOUR")
                        .index(2)
                        .required(true)
                        .help("The hex code for the material's new colour"),
                ),
        )
        .subcommand(
            SubCommand::with_name("edit-property")
                .about("Change a property on an existing material mapping")
                .arg(
                    Arg::with_name("MAT_NAME")
                        .index(1)
                        .required(true)
                        .help("The name of the material to adjust"),
                )
                .arg(
                    Arg::with_name("PROP_NAME")
                        .index(2)
                        .required(true)
                        .help("The name of the property to adjust"),
                )
                .arg(
                    Arg::with_name("PROP_VALUE")
                        .index(3)
                        .required(true)
                        .help("The new value to assign to the property"),
                ),
        )
        .subcommand(
            SubCommand::with_name("add-property")
                .about("Add a property to an existing material mapping")
                .arg(
                    Arg::with_name("MAT_NAME")
                        .index(1)
                        .required(true)
                        .help("The name of the material to adjust"),
                )
                .arg(
                    Arg::with_name("PROP_NAME")
                        .index(2)
                        .required(true)
                        .help("The name of the property to adjust"),
                )
                .arg(
                    Arg::with_name("PROP_VALUE")
                        .index(3)
                        .required(true)
                        .help("The new value to assign to the property"),
                ),
        )
        .get_matches();

    if let None = matches.subcommand_name() {
        eprintln!("No subcommand specified");
        eprintln!("{}", matches.usage());
        return;
    }

    let mat_map_path = matches.value_of("MAP_FILE").unwrap();
    let mut mat_map = MatMap::load_or_create(Path::new(mat_map_path));

    // Process subcommand
    match matches.subcommand() {
        ("export-palette", Some(subcommand)) => {
            let png_path = subcommand.value_of("PNG_FILE").unwrap();
            match export_palette_png(Path::new(png_path), &mat_map.mappings) {
                Ok(()) => (),
                Err(s) => eprintln!("{}", s),
            }
        }
        ("show", Some(subcommand)) => {
            let mut names = Vec::new();
            let mut colours = Vec::new();
            if let Some(name_values) = subcommand.values_of("name") {
                for name in name_values {
                    names.push(name.to_string());
                }
            }

            if let Some(colour_values) = subcommand.values_of("colour") {
                for colour in colour_values {
                    if let Ok(colour) = MatMapEntry::clean_colour_string(colour) {
                        colours.push(colour);
                    } else {
                        return;
                    }
                }
            }

            mat_map.show_mappings(&names, &colours)
        }
        ("add", Some(subcommand)) => {
            let name = subcommand.value_of("NAME").unwrap();
            let colour = subcommand.value_of("COLOUR").unwrap();

            let mut properties = Vec::new();
            if let Some(property_values) = subcommand.values_of("property") {
                for property in property_values.collect::<Vec<_>>().chunks(2) {
                    let prop_name = property[0];
                    let prop_value = property[1];
                    properties.push(PropertyEntry::new(&prop_name, &prop_value));
                }
            }
            if let Ok(colour) = MatMapEntry::clean_colour_string(&colour) {
                if let Ok(_) = mat_map.add_mapping(name, &colour, &properties) {
                    mat_map.serialise(Path::new(mat_map_path));
                }
            }
        }
        ("rename", Some(subcommand)) => {
            let old_name = subcommand.value_of("CURRENT_NAME").unwrap();
            let new_name = subcommand.value_of("NEW_NAME").unwrap();
            if let Ok(_) = mat_map.rename_mapping(old_name, new_name) {
                mat_map.serialise(Path::new(mat_map_path));
            }
        }
        ("recolour", Some(subcommand)) => {
            let name = subcommand.value_of("NAME").unwrap();
            let colour = subcommand.value_of("COLOUR").unwrap();
            if let Ok(colour) = MatMapEntry::clean_colour_string(&colour) {
                if let Ok(_) = mat_map.recolour_mapping(name, &colour) {
                    mat_map.serialise(Path::new(mat_map_path));
                }
            }
        }
        ("edit-property", Some(subcommand)) => {
            let mat_name = subcommand.value_of("MAT_NAME").unwrap();
            let prop_name = subcommand.value_of("PROP_NAME").unwrap();
            let prop_value = subcommand.value_of("PROP_VALUE").unwrap();
            if let Ok(_) = mat_map.edit_property(mat_name, prop_name, prop_value) {
                mat_map.serialise(Path::new(mat_map_path));
            }
        }
        ("add-property", Some(subcommand)) => {
            let mat_name = subcommand.value_of("MAT_NAME").unwrap();
            let prop_name = subcommand.value_of("PROP_NAME").unwrap();
            let prop_value = subcommand.value_of("PROP_VALUE").unwrap();
            if let Ok(_) = mat_map.add_property(mat_name, prop_name, prop_value) {
                mat_map.serialise(Path::new(mat_map_path));
            }
        }
        _ => unreachable!(),
    }
}
