use crate::{rgb_from_hex, MatMapEntry};
use png::HasParameters;
use rusttype::{point, Font, PositionedGlyph, Scale};
use std::{fs::File, io::BufWriter, path::Path};

const MAPPING_HEIGHT: usize = 16;
const MAPPING_PADDING: usize = 4;
const MAPPING_COLOUR_BOUNDS: usize = 16;
const FONT: &[u8] = include_bytes!("./DejaVuSansMono.ttf");

struct PaletteImage {
    width: usize,
    data: Vec<u8>,
}

impl PaletteImage {
    fn new(width: usize, height: usize, fill: u8) -> Self {
        Self {
            width,
            data: vec![fill; width * height * 3],
        }
    }

    fn render_mapping(
        &mut self,
        v_pos: usize,
        [r, g, b]: [u8; 3],
        glyphs: &[PositionedGlyph],
    ) {
        // Render colour blob
        for y in 0..MAPPING_COLOUR_BOUNDS {
            for x in 0..MAPPING_COLOUR_BOUNDS {
                let x = MAPPING_PADDING + x;
                let y = v_pos + y;
                let byte_idx = (x + y * self.width) * 3;
                self.data[byte_idx + 0] = r;
                self.data[byte_idx + 1] = g;
                self.data[byte_idx + 2] = b;
            }
        }

        // Write glyphs to data
        for glyph in glyphs.iter() {
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                let bb_min_x = bounding_box.min.x as usize;
                let bb_min_y = bounding_box.min.y as usize;
                glyph.draw(|x, y, v| {
                    let x = MAPPING_PADDING
                        + MAPPING_COLOUR_BOUNDS
                        + MAPPING_PADDING
                        + bb_min_x
                        + x as usize;
                    let y = v_pos + bb_min_y + y as usize;
                    let byte_idx = (x + y * self.width) * 3;
                    let v = (255.0 - v * 255.0) as u8;
                    self.data[byte_idx + 0] = v;
                    self.data[byte_idx + 1] = v;
                    self.data[byte_idx + 2] = v;
                });
            }
        }
    }
}

/// Calculate the total width of a string of glyphs
fn glyphs_width(glyphs: &[PositionedGlyph]) -> usize {
    let min_x = glyphs
        .first()
        .map(|g| g.pixel_bounding_box().unwrap().min.x)
        .unwrap();

    let max_x = glyphs
        .last()
        .map(|g| g.pixel_bounding_box().unwrap().max.x)
        .unwrap();

    (max_x - min_x) as usize
}

pub fn export_palette_png(output_path: &Path, mappings: &[MatMapEntry]) -> Result<(), String> {
    // Don't bother doing anything if there are no mappings to export
    if mappings.is_empty() {
        return Err("No mappings to export".to_string());
    }

    // Define font details
    let font = Font::from_bytes(FONT).map_err(|_| "Failed to construct font".to_string())?;
    let font_scale = Scale::uniform(16.0);
    let font_v_metrics = font.v_metrics(font_scale);
    let font_ascent = font_v_metrics.ascent;

    // Create text glyphs for each mapping name, and record the width of the longest string of glyphs
    let mut max_glyphs_width = 0;
    let mapping_name_glyphs = {
        let mut mapping_name_glyphs = Vec::new();
        for mapping in mappings.iter() {
            let glyphs: Vec<_> = font
                .layout(mapping.name(), font_scale, point(0.0, font_ascent))
                .collect();
            max_glyphs_width = usize::max(max_glyphs_width, glyphs_width(&glyphs));
            mapping_name_glyphs.push(glyphs);
        }
        mapping_name_glyphs
    };

    // Create image canvas
    let canvas_width = MAPPING_PADDING
        + MAPPING_COLOUR_BOUNDS
        + MAPPING_PADDING
        + max_glyphs_width
        + MAPPING_PADDING;
    let canvas_height = MAPPING_PADDING + mappings.len() * (MAPPING_HEIGHT + MAPPING_PADDING);

    let mut image = PaletteImage::new(canvas_width, canvas_height, 255);

    // Render colour blobs and text
    for (idx, mapping) in mappings.iter().enumerate() {
        let rgb = rgb_from_hex(mapping.colour());
        let v_pos = MAPPING_PADDING + idx * (MAPPING_HEIGHT + MAPPING_PADDING);
        image.render_mapping(v_pos, rgb, &mapping_name_glyphs[idx]);
    }

    // Encode image data as PNG and output to file
    let file = File::create(output_path)
        .map_err(|_| format!("Failed to open file '{}'", output_path.to_string_lossy()))?;
    let mut writer = BufWriter::new(file);

    let mut encoder = png::Encoder::new(&mut writer, canvas_width as u32, canvas_height as u32);
    encoder.set(png::ColorType::RGB).set(png::BitDepth::Eight);

    let mut writer = encoder.write_header().map_err(|_| {
        format!(
            "Failed to write PNG header to file '{}'",
            output_path.to_string_lossy()
        )
    })?;

    writer.write_image_data(&image.data).map_err(|_| {
        format!(
            "Failed to write PNG data to file '{}'",
            output_path.to_string_lossy()
        )
    })?;

    Ok(())
}
